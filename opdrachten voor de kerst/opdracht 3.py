# -*- coding: utf-8 -*-
"""
Created on Tue Dec  6 16:43:27 2022

@author: BeTra
"""

#from scipy.linalg import expm, logm
from scipy import signal
import matplotlib.pyplot as plt
import mbrtc
import numpy as np


"""
_/_/_/_/_/_/_/

Model: Example A.3 harmonix oscillator

G(s) = 1 /(S²+1)

state space model:
    
         | 0 1 |     | 0 |
Dx/Dt =  |-1 0 | x + | 1 |u

y = (1 0)x
_/_/_/_/_/_/_/

"""
#array waarden:
A_array = np.array([[0. , 1.],
           [-1., 0]])
B_array = np.array([[0.],
           [1]])
C_array = np.array([[1,0]])
D_array = 0

begintoestand = np.array([1,3])

sample_time = 0.25

class systeem_symulatie:
    
    def __init__(self, A,B,C,D,sample_time):

        self.__A = A
        self.__B = B
        self.__C = C
        self.__D = D
        self.__system_ss = signal.StateSpace(A,B,C,D)
        self.__sample_time = sample_time
        
    def __calculate_step_response(self):
        """
        calculate the step response vallues

        Returns
        -------
        Array of step response vallues

        """
        step_response = signal.step(self.__system_ss)
        return(step_response)
    
    def __calculate_sim_values(self, time, X0=[0,0]):
        """
        Calculates the the output of a simulation based ont he signal.lsim function

        Parameters
        ----------
        time : float
            total simulation time

        Returns
        -------
        time_array : Numpy.Array
            array with equal spaced time intervalts
        y : numpy.array
            array with output values

        """
        time_array = self.__create_time_array(self.__sample_time, time)
        input_signal = np.ones_like(time_array)
        tout, y, x = signal.lsim(self.__system_ss, input_signal, time_array, X0)
        return time_array,y
    
    def __create_time_array(self, sample_time, time):
        """
        Generates an array with equaly spaced time interfals

        Parameters
        ----------
        sample_time : float
            sample time of simulation
        time : float
            total time of the simultion

        Returns
        -------
        numpy array of floats of with format time,output

        """
        num = int(time/sample_time) 
        return(np.linspace(0., time, num))
    
    def __get_continues_poles(self):
        """
        calculates the poles based on a continues time signal

        Returns
        -------
        Numpy.array of floats with the poles
            DESCRIPTION.

        """
        continues_poles = self.__system_ss.poles
        print(continues_poles.ndim)
        return np.array([continues_poles[0],continues_poles[1]])
    
    def __calculate_place_poles(self):
        """
        Calculate the values of the desired and calculated poles

        Returns
        -------
        fsf2 : POINTER
            pointer to object with the values.

        """
        poles = self.__get_continues_poles()
        fsf2 = signal.place_poles(self.__A, self.__B, poles, method='YT')
        return fsf2
        
    def __calculate_state_feedback_gain(self):
        """
        calculates the gain matrix of the system

        Returns
        -------
        gain_matrix : numpy.array
            gain matrix

        """
        poles = self.__get_continues_poles()
        fsf = signal.place_poles(self.__A, self.__B, poles,method='YT')
        gain_matrix = fsf.gain_matrix
        return gain_matrix
    
    def __c2d_ZOH(self):
        """
        

        Returns
        -------
        TYPE
            DESCRIPTION.

        """
        return mbrtc.c2d_zoh(self.__A ,self.__B, self.__C, 
                             np.array([[self.__D],[0.0]]), self.__sample_time)
    
    def __calculate_close_loop_feedback(self, A,B):
        """
        

        Parameters
        ----------
        A : TYPE
            DESCRIPTION.
        B : TYPE
            DESCRIPTION.

        Returns
        -------
        A_feedback : TYPE
            DESCRIPTION.

        """

        contrability_matrix = mbrtc.ctrb(A,B)
        print(A[0], B[0], contrability_matrix[0] )
        A_feedback = A - B[0] * contrability_matrix[0]
        return A_feedback
    
        
    
    def plot_step(self):
        """
        plots the step response of the given system

        Returns
        -------
        None.

        """
        t,y = self.__calculate_step_response()
        plt.plot(t,y)
        plt.xlabel('Time [s]')
        plt.ylabel('Amplitude')
        plt.title('Stap responsie systeem')
        plt.grid()
        plt.show()
    
    def check_is_observable_and_reachable(self):
        """
        checks if the system is both reachable and observable.

        Returns
        -------
        None.

        """
        reachable = mbrtc.is_reachable(self.__A, self.__B)
        observable = mbrtc.is_observable(self.__A, self.__C)
        reachable_string = 'Systeem is niet reachable'
        observable_string = 'Systeem is niet observable'
        if reachable:
            reachable_string = 'Systeem is reachable'
            
        if observable:
            observable_string = 'Systeem is observable'
            
        print(reachable_string + ' & ' + observable_string)

    def plot_poles(self):
        """
        make a plot of both the desired and actual poles based on the system. 

        Returns
        -------
        None.
        
        sows plot of simulation

        """
        fsf = self.__calculate_place_poles()
        t = np.linspace(0, 2*np.pi, 401)
        plt.plot(np.cos(t), np.sin(t), 'k--')  # unit circle
        plt.plot(fsf.requested_poles.real, fsf.requested_poles.imag,
                 'bo', label='gewenste ligging')
        plt.plot(fsf.computed_poles.real, fsf.computed_poles.imag, 'r+',
                 label='berekende ligging')
        plt.grid()
        plt.axis('image')
        plt.axis([-1.1, 1.1, -1.1, 1.1])
        plt.ylabel('Imaginaire as')
        plt.xlabel('Reele as')
        plt.legend(bbox_to_anchor=(1.05, 1), loc=2, numpoints=1)
        plt.show()
        
    def plot_simulation(self, time, X0=[0,0]):
        """
        function to simulate and plot the result based on the continues time signal

        Parameters
        ----------
        time : float
            total simulation run time

        Returns
        -------
        None
        
        shows plot of simulation. 

        """
        time_array, y =self.__calculate_sim_values(time, X0)
        plt.plot(time_array,y)
        plt.grid(alpha=0.2)
        plt.xlabel('tijd [s]')
        plt.show()
    
    def print_gain_matrix(self):
        """
        prints the gain matrix to python terminal

        Returns
        -------
        None.

        """
        print(self.__calculate_state_feedback_gain())
        
    def plot_openloop_vs_closedloop(self,sim_time, X0):
        figure, axis = plt.subplots(2)
        
        #setup discrete plot for the openloop feedback
        A,B,C,D = self.__c2d_ZOH()
        time_array = self.__create_time_array(self.__sample_time, sim_time)
        input_signal = np.ones_like(time_array)
        
        #setup discrete plot for the closeloop feedback
        A_feedback = self.__calculate_close_loop_feedback(A,B)
        
        
        #simulate the open loop
        output_signal_open_loop = mbrtc.sim(A,B,C,D, input_signal, x0=X0)
        
        #simulate the close loop
        output_signal_close_loop = mbrtc.sim(A_feedback, B, C, D, input_signal, x0=X0)
        
        figure.suptitle('openloop vs closeloop')
        #plot open loop output
        axis[0].plot(time_array, output_signal_open_loop[0])
        axis[0].grid(alpha=0.2)
        axis[0].set_title('Open loop feedback')
        axis[0].set(xlabel='time [s]')
        
        #plot closed loop output
        axis[1].plot(time_array, output_signal_close_loop[0])
        axis[1].grid(alpha=0.2)
        axis[1].set_title('close loop feedback')
        axis[1].set(xlabel='time [s]')
        

        
        plt.show()
        
        
        

# initialiseer de simulatie en geeft de simulatie waarden mee
systeem = systeem_symulatie(A_array, B_array, C_array, D_array, sample_time) 

#plot de step responsie
systeem.plot_step()
#check of the matrix observeerbaar is
systeem.check_is_observable_and_reachable()
#plot de gewilde en berekenede polen van het continue systeem
systeem.plot_poles()
#simuleer het systeem geduurende 5 seconden (kan meer zijn)
systeem.plot_simulation(5, begintoestand)
#print the gain matrix
systeem.print_gain_matrix()

#simulate open and close loop
systeem.plot_openloop_vs_closedloop(5, begintoestand)









