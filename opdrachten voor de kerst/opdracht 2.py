# -*- coding: utf-8 -*-
"""
Created on Tue Nov 24 12:43:09 2020

@author: User
"""

from scipy.linalg import expm, logm
from scipy import signal
import matplotlib.pyplot as plt
from mbrtc import *
from scipy.signal import step, impulse, lti
import numpy as np

sample_time = 0.22 #seconden

simulariteids_matrix = [[1,1],[1,-1]]

def overbrenging_motor():
    num = [1.]
    den = [1.,1.,0.]
    return(signal.tf2ss(num,den))

def plot_bode_plot(sys):
    w, mag, phase = sys.bode()
    plt.figure('bode')
    plt.semilogx(w, mag)    # Bode magnitude plot
    plt.semilogx(w, phase)  # Bode phase plot
    plt.show()
    
def plot(signaal,name=''):
    plt.figure(name)
    plt.plot(signaal[0], signaal[1])
    plt.show()
    
def provide_continues_signal_responses(system, display_name='-'):
    print("Providing feedback of simulation : " , display_name)
    signal_system = signal.StateSpace(system[0],system[1],system[2],system[3])
    print("Poles found: " ,signal_system.poles)
    print("Zeros found: " ,signal_system.zeros )
    step_system = step(signal_system)
    plot(step_system,'step')
    plot(impulse(signal_system), 'impulse')
    plot_bode_plot(signal_system)

def plot_sim_intersample(system,sample_time,duration, title = ''):
    discreet_time_sample_array = np.arange(0,duration,step=sample_time)
    result_time, results = sim_intersample(system[0], system[1], system[2], 
                                           system[3], sample_time, 10, 
                                           spike(len(discreet_time_sample_array)).T, discreet_time_sample_array)
    plt.figure(f'Intersample  {title}')
    plt.plot(results, result_time)
    plt.show()

def plot_ss_serie(state_space_1, state_space_2):
     serie_state_space = ss_series(state_space_1[0], state_space_1[1], state_space_1[2], state_space_1[3], state_space_2[0], state_space_2[1], state_space_2[2], state_space_2[3])
     plot_sim_intersample(serie_state_space, sample_time, 5, title = 'state space serie')
     
def plot_ss_paralel(state_space_1, state_space_2):
    paralel_state_space =  ss_parallel(state_space_1[0], state_space_1[1], state_space_1[2], state_space_1[3], state_space_2[0], state_space_2[1], state_space_2[2], state_space_2[3])
    plot_sim_intersample(paralel_state_space, sample_time, 5, title = 'state space paralel')
    
def plot_ss_feedback(state_space_1, state_space_2):
    feedback_state_space =  ss_feedback(state_space_1[0], state_space_1[1], state_space_1[2], state_space_1[3], state_space_2[0], state_space_2[1], state_space_2[2], state_space_2[3])
    plot_sim_intersample(feedback_state_space, sample_time, 5, title = 'state space feedback')
    
continue_sys = overbrenging_motor()

zero_order_system_state_space= [np.zeros((0,0)),np.zeros((0,1)),np.zeros((1,0)),np.array([[1.]])]

discreet_sys = c2d_zoh(continue_sys[0],continue_sys[1],continue_sys[2],continue_sys[3], sample_time)
discreet_uitgangssignaal = sim(discreet_sys[0],discreet_sys[1],discreet_sys[2],discreet_sys[3],spike(100).T)

continue_sys_getransformeerd= similarity_trans(continue_sys[0],continue_sys[1],continue_sys[2],continue_sys[3],simulariteids_matrix)



provide_continues_signal_responses(continue_sys, 'continue signaal')
provide_continues_signal_responses(discreet_sys, 'discreet systeem')
provide_continues_signal_responses(continue_sys_getransformeerd, 'getransformeerd')
plot_sim_intersample(continue_sys_getransformeerd, sample_time, 5)
plot_ss_serie(discreet_sys, zero_order_system_state_space)
plot_ss_paralel(discreet_sys, zero_order_system_state_space)
plot_ss_feedback(discreet_sys, zero_order_system_state_space)

